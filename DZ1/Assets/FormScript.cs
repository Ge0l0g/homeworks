﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FormScript : MonoBehaviour
{
    [SerializeField]
    private Button SaveButton;
    [SerializeField]
    private Button CancelButton;

    [SerializeField]
    private Dropdown Type;

    [SerializeField]
    private InputField Title;
    [SerializeField]
    private InputField Description;
    [SerializeField]
    private InputField Effort;

    [SerializeField]
    private Toggle flag;

    // Start is called before the first frame update
    void Start()
    {
        //SaveButton = GameObject.Find("BSave").GetComponent<Button>();
        CancelButton.onClick.AddListener(CancelClick);
    }

    private void CancelClick()
    {
        Title.text = "";
        Description.text = "";
        Effort.text = "";
        Type.value = 0;
        flag.isOn = false;
    }
    // Update is called once per frame
    void Update()
    {
        if(Title.text != "" && Description.text != "" && Effort.text != "" && flag.isOn == true)
            SaveButton.interactable = true;
        else
            SaveButton.interactable = false;
    }
}
