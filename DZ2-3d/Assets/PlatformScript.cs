﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour
{
    private bool move_left;
    private bool move_right;
    private int score;
    // Start is called before the first frame update
    void Start()
    {
        // Initialize game parameters
        score = 0;
        move_left = false;
        move_right = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
            move_left = true;
        if (Input.GetKeyUp(KeyCode.A))
            move_left = false;
        if (Input.GetKeyDown(KeyCode.D))
            move_right = true;
        if (Input.GetKeyUp(KeyCode.D))
            move_right = false;

        if (move_right == true && move_left == true)
        {
            move_right = false;
            move_left = false;
        }
        else if (move_right == true)
            transform.Translate(1, 0, 0);
        else if (move_left == true)
            transform.Translate(-1, 0, 0);
    }

    void OnCollisionEnter(Collision myCollision)
    {
        if (myCollision.gameObject.name == "Ball")
        {
            score += 1;
            Debug.Log("Hits with ball:" + score.ToString());
        }
    }
}
